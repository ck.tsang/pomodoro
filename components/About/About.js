import React from "react";

export default function About() {
  return (
    <div className="mx-auto mt-24 flex w-11/12 justify-center p-5 text-white">
      <div>
        <h3 className="text-xl font-semibold sm:text-2xl">
          <span className="border-b-4 border-red-400">What</span> is the
          Pomodoro Technique?
        </h3>
        <p className="mt-4 text-lg tracking-wide opacity-75">
          The Pomodoro Technique is a time management system created by
          Francesco Cirillo for a more productive way to work and study. The
          technique uses a timer to break down work into intervals,
          traditionally 25 minutes in length, separated by short breaks. Each
          interval is known as a pomodoro, the Italian word for 'tomato', after
          the tomato-shaped kitchen timer that Cirillo used as a university
          student.
        </p>
      </div>
    </div>
  );
}
