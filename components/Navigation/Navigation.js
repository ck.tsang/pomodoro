import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGear, faClock } from "@fortawesome/free-solid-svg-icons";

export default function Navigation() {
  return (
    <nav className="mx-auto flex w-11/12 justify-between pt-4 text-white ">
      <div className="flex cursor-pointer items-center space-x-2">
        <FontAwesomeIcon icon={faClock} />
        <h1 className="text-lg font-semibold text-red-500">Pomodoro</h1>
      </div>
      <div>
        <FontAwesomeIcon icon={faGear} size="xl" className="cursor-pointer" />
      </div>
    </nav>
  );
}
