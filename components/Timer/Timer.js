import React from "react";

export default function Timer() {
  const options = ["Pomodoro", "Short Break", "Long Break"];
  return (
    <div className="mx-auto mt-8 flex w-10/12 flex-col items-center justify-center pt-4 text-white">
      <div className="flex space-x-8">
        {options.map((option, index) => {
          return (
            <h3 className="rounded bg-gray-600 py-1 px-2" key={index}>
              {option}
            </h3>
          );
        })}
      </div>

      <div className="my-10">
        <h2 className="select-none text-8xl font-bold">25:00</h2>
      </div>
      <button className="rounded-lg bg-white px-16 py-2 text-2xl font-bold text-blue-500">
        Start
      </button>
    </div>
  );
}
