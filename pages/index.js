import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGear } from "@fortawesome/free-solid-svg-icons";
import Navigation from "../components/Navigation/Navigation";
import Timer from "../components/Timer/Timer";
import About from "../components/About/About";

export default function Home() {
  return (
    <div className="min-h-screen bg-gray-900">
      <div className="mx-auto min-h-screen max-w-2xl">
        <Navigation />
        <Timer />
        <About />
      </div>
    </div>
  );
}
